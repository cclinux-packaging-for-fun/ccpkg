import os
import unittest

from ccpkg import lookaside


class TestIsDistGit(unittest.TestCase):
  def setUp(self):
    cwd = os.path.dirname(os.path.realpath(__file__))
    self.fixtures_dir = '%s/fixtures/layouts' % cwd

  def test_dist_git(self):
    path = '%s/dist-git' % self.fixtures_dir
    assert lookaside.is_dist_git(path)

  def test_dist_git_false(self):
    path = '%s/srpm' % self.fixtures_dir
    assert not lookaside.is_dist_git(path)
