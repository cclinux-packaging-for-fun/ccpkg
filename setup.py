from setuptools import setup

def get_requirements(requirements_file="requirements.txt"):
    """Get the contents of a file listing the requirements.

    :arg requirements_file: path to a requirements file
    :type requirements_file: string
    :returns: the list of requirements, or an empty list if
              `requirements_file` could not be opened or read
    :return type: list
    """

    lines = open(requirements_file).readlines()
    dependencies = []
    for line in lines:
        maybe_dep = line.strip()
        if maybe_dep.startswith("#"):
            # Skip pure comment lines
            continue
        if maybe_dep.startswith("git+"):
            # VCS reference for dev purposes, expect a trailing comment
            # with the normal requirement
            __, __, maybe_dep = maybe_dep.rpartition("#")
        else:
            # Ignore any trailing comment
            maybe_dep, __, __ = maybe_dep.partition("#")
        # Remove any whitespace and assume non-empty results are dependencies
        maybe_dep = maybe_dep.strip()
        if maybe_dep:
            dependencies.append(maybe_dep)
    return dependencies

setup(
    name="ccpkg",
    version='0.0.1',
    author="Brian Stinson",
    author_email="bstinson@redhat.com",
    description="CentOS Plugin to rpkg for managing RPM package sources",
    license="GPLv2+",
    package_dir={'': 'src'},
    packages=['ccpkg'],
    install_requires=get_requirements(),
    scripts=['src/bin/ccpkg', 'src/bin/ccpkg-sig'],
)
