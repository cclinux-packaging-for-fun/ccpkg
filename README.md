# ccpkg

ccpkg is a fork of centpkg to be updated for working with Circle Linux build
system.

**Please note that, this is not an official packaging tool, just for personal
usage for fun.**

A few common rpkg commands works as normal, like `clone`, `srpm`, `mockbuild`.

## Prepare execution environment

Create a Python virtual environment.

```bash
sudo dnf install python3-rpm
cd path/to/ccpkg
python3 -m venv --system-site-packages .venv
. .venv/bin/activate
python3 setup.py develop
```

now, you can do following for example,

```
ccpkg --config path/to/ccpkg/src/ccpkg.conf clone -a rpms/emacs
cd emacs
ccpkg --config path/to/ccpkg/src/ccpkg.conf srpm
ccpkg --config path/to/ccpkg/src/ccpkg.conf mockbuild
```

## Generate mock config

```bash
ccpkg --config path/to/ccpkg/src/ccpkg.conf mock-config --target dist-circle8
# Save output to /etc/mock/cclinux-8-x86_64.cfg
```

You can list build targets with command:

```bash
koji --profile cclinux list-targets
```

The `cclinux` koji profile can be found
[here](https://gitlab.com/cclinux-packaging-for-fun/cclinux-packager).
