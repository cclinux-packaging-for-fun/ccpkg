import unittest
import unittest.mock

import git

from .mixins import CatchWarningsMixin
from ccpkg import DistGitDirectory
from ccpkg import git as ccpkg_git


class TestDistGitNothing(unittest.TestCase):
    def test_distgit_emptystring(self):
        with self.assertRaises(TypeError):
            d = DistGitDirectory()

class TestDistGitInvalid(unittest.TestCase):
    def test_invalid_branchstring_raises(self):
        self.branchstring = 'nope-not-a-branch'

        with self.assertRaises(ValueError):
            self.d = DistGitDirectory(self.branchstring)

class TestDistgitOnlySig(unittest.TestCase):
    def setUp(self):
        self.branchstring = 'c7-sig-cloud'
        self.d = DistGitDirectory(self.branchstring)

    def test_signame_gets_set(self):
        self.assertEqual(self.d.signame, 'cloud')

    def test_centosversion_gets_set(self):
        self.assertEqual(self.d.centosversion, '7')

    def test_projectname_gets_set(self):
        self.assertEqual(self.d.projectname, None)

    def test_releasename_gets_set(self):
        self.assertEqual(self.d.releasename, None)

    def test_target_gets_set(self):
        self.assertEqual(self.d.target, 'cloud7-common-el7')

class TestDistgitSigAndCommon(unittest.TestCase):
    def setUp(self):
        self.branchstring = 'c7-sig-cloud-common'
        self.d = DistGitDirectory(self.branchstring)

    def test_signame_gets_set(self):
        self.assertEqual(self.d.signame, 'cloud')

    def test_centosversion_gets_set(self):
        self.assertEqual(self.d.centosversion, '7')

    def test_projectname_gets_set(self):
        self.assertEqual(self.d.projectname, None)

    def test_releasename_gets_set(self):
        self.assertEqual(self.d.releasename, None)

    def test_target_gets_set(self):
        self.assertEqual(self.d.target, 'cloud7-common-el7')

class TestDistgitSigAndProject(unittest.TestCase):
    def setUp(self):
        self.branchstring = 'c7-sig-cloud-openstack'
        self.d = DistGitDirectory(self.branchstring)

    def test_signame_gets_set(self):
        self.assertEqual(self.d.signame, 'cloud')

    def test_centosversion_gets_set(self):
        self.assertEqual(self.d.centosversion, '7')

    def test_projectname_gets_set(self):
        self.assertEqual(self.d.projectname, 'openstack')

    def test_releasename_gets_set(self):
        self.assertEqual(self.d.releasename, None)

    def test_target_gets_set(self):
        self.assertEqual(self.d.target, 'cloud7-openstack-common-el7')

class TestDistgitSigProjectAndCommon(unittest.TestCase):
    def setUp(self):
        self.branchstring = 'c7-sig-cloud-openstack-common'
        self.d = DistGitDirectory(self.branchstring)

    def test_signame_gets_set(self):
        self.assertEqual(self.d.signame, 'cloud')

    def test_centosversion_gets_set(self):
        self.assertEqual(self.d.centosversion, '7')

    def test_projectname_gets_set(self):
        self.assertEqual(self.d.projectname, 'openstack')

    def test_releasename_gets_set(self):
        self.assertEqual(self.d.releasename, None)

    def test_target_gets_set(self):
        self.assertEqual(self.d.target, 'cloud7-openstack-common-el7')

class TestDistgitSigProjectAndRelease(unittest.TestCase):
    def setUp(self):
        self.branchstring = 'c7-sig-cloud-openstack-kilo'
        self.d = DistGitDirectory(self.branchstring)

    def test_signame_gets_set(self):
        self.assertEqual(self.d.signame, 'cloud')

    def test_centosversion_gets_set(self):
        self.assertEqual(self.d.centosversion, '7')

    def test_projectname_gets_set(self):
        self.assertEqual(self.d.projectname, 'openstack')

    def test_releasename_gets_set(self):
        self.assertEqual(self.d.releasename, 'kilo')

    def test_target_gets_set(self):
        self.assertEqual(self.d.target, 'cloud7-openstack-kilo-el7')

class TestDistgitC7DistroBranch(unittest.TestCase):
    def setUp(self):
        self.branchstring = 'c7'
        self.d = DistGitDirectory(self.branchstring)

    def test_signame_gets_set(self):
        self.assertEqual(self.d.signame, 'centos')

    def test_centosversion_gets_set(self):
        self.assertEqual(self.d.centosversion, '7')

    def test_projectname_gets_set(self):
        self.assertEqual(self.d.projectname, None)

    def test_releasename_gets_set(self):
        self.assertEqual(self.d.releasename, None)

    def test_target_gets_set(self):
        self.assertEqual(self.d.target, 'c7')

class TestDistgitStream9Module(unittest.TestCase):
    def setUp(self):
        self.branchstring = '3.0-rhel-9.0.0-beta'
        self.d = DistGitDirectory(self.branchstring)

    def test_signame_gets_set(self):
        self.assertEqual(self.d.signame, 'centos')

    def test_centosversion_gets_set(self):
        self.assertEqual(self.d.centosversion, '9')

    def test_projectname_gets_set(self):
        self.assertEqual(self.d.projectname, None)

    def test_releasename_gets_set(self):
        self.assertEqual(self.d.releasename, None)

    def test_target_gets_set(self):
        self.assertEqual(self.d.target, 'c9s-candidate')

class TestDistgitStream9ModuleComponent(unittest.TestCase):
    def setUp(self):
        self.branchstring = 'stream-container-tools-3.0-rhel-9.0.0-beta'
        self.d = DistGitDirectory(self.branchstring)

    def test_signame_gets_set(self):
        self.assertEqual(self.d.signame, 'centos')

    def test_centosversion_gets_set(self):
        self.assertEqual(self.d.centosversion, '9')

    def test_projectname_gets_set(self):
        self.assertEqual(self.d.projectname, None)

    def test_releasename_gets_set(self):
        self.assertEqual(self.d.releasename, None)

    def test_target_gets_set(self):
        self.assertEqual(self.d.target, 'c9s-candidate')

class TestDistgitC6DistroBranch(unittest.TestCase):
    def setUp(self):
        self.branchstring = 'c6'
        self.d = DistGitDirectory(self.branchstring)

    def test_signame_gets_set(self):
        self.assertEqual(self.d.signame, 'centos')

    def test_centosversion_gets_set(self):
        self.assertEqual(self.d.centosversion, '6')

    def test_projectname_gets_set(self):
        self.assertEqual(self.d.projectname, None)

    def test_releasename_gets_set(self):
        self.assertEqual(self.d.releasename, None)

    def test_target_gets_set(self):
        self.assertEqual(self.d.target, 'c6')

class TestDistgitC6PlusDistroBranch(unittest.TestCase):
    def setUp(self):
        self.branchstring = 'c6-plus'
        self.d = DistGitDirectory(self.branchstring)

    def test_signame_gets_set(self):
        self.assertEqual(self.d.signame, 'centos')

    def test_centosversion_gets_set(self):
        self.assertEqual(self.d.centosversion, '6')

    def test_projectname_gets_set(self):
        self.assertEqual(self.d.projectname, 'plus')

    def test_releasename_gets_set(self):
        self.assertEqual(self.d.releasename, None)

    def test_target_gets_set(self):
        self.assertEqual(self.d.target, 'c6-plus')

class TestOldGitBranch(unittest.TestCase, CatchWarningsMixin):
    def test_old_branch_warns(self):
       with self.assertWarns(DeprecationWarning):
           branchstring = 'virt7'
           d = DistGitDirectory(branchstring)

class TestIsFork(unittest.TestCase):
    def setUp(self):
        self.branchstring = 'c9s'

    def test_none(self):
        d = DistGitDirectory(self.branchstring)
        self.assertFalse(d.is_fork())

    @unittest.mock.patch.object(ccpkg_git.repo.Repo, 'remotes', new=dict(origin=type('Remote', (object,), {'urls': ['ssh://git@gitlab.com/someone/ccpkg.git']})))
    @unittest.mock.patch.object(ccpkg_git.repo.Repo, '__init__', new=lambda s, p: None)
    def test_fork_url(self):
        d = DistGitDirectory(self.branchstring, 'binutils')
        self.assertTrue(d.is_fork())
    
    @unittest.mock.patch.object(ccpkg_git.repo.Repo, 'remotes', new=dict(origin=type('Remote', (object,), {'urls': ['git+ssh://git@gitlab.com/redhat/centos-stream/rpms/binutils.git']})))
    @unittest.mock.patch.object(ccpkg_git.repo.Repo, '__init__', new=lambda s, p: None)
    def test_upstream_url(self):
        d = DistGitDirectory(self.branchstring, 'binutils')
        self.assertFalse(d.is_fork())
