import unittest

from ccpkg import utils


class TestGetRepoName(unittest.TestCase):
  def test_rpms_org(self):
    assert utils.get_repo_name('rpms/time') == 'rpms/time'

  def test_gitlab_fork(self):
    assert utils.get_repo_name('someuser/time') == 'rpms/time'

  def test_ccpkg_fork(self):
    assert utils.get_repo_name('someuser/centos_rpms_binutils') == 'rpms/binutils'

  def test_ssh_fork(self):
    assert utils.get_repo_name('git@gitlab.com:nickc2/binutils') == 'rpms/binutils'


if __name__ == '__main__':
    unittest.main()
